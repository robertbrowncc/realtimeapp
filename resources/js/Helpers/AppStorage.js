class AppStorage {
    storeToken(token) {
        localStorage.setItem('token', token);
    }
    storeUser(user) {
        localStorage.setItem('user', user);
    }
    storeUserId(user_id) {
        localStorage.setItem('user_id', user_id);
    }
    store(user, user_id, token) {
        this.storeToken(token);
        this.storeUserId(user_id);
        this.storeUser(user);
    }
    clear() {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        localStorage.removeItem('user_id');
    }
    getToken() {
        return localStorage.getItem('token');
    }
    getUser() {
        return localStorage.getItem('user');
    }
    getUserId() {
        return localStorage.getItem('user_id');
    }
}


export default AppStorage = new AppStorage();

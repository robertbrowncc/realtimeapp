import Token from "./Token";
import AppStorage from "./AppStorage";

class User {
    login(data) {
        return axios.post('/api/auth/login', data).then(res => {
            this.responseAfterLogin(res);
        });
    }

    responseAfterLogin(res) {
        const access_token = res.data.access_token;
        const user = res.data.user;
        const user_id = res.data.user_id;

        if (Token.isValid(access_token)) {
            AppStorage.store(user, user_id, access_token);
        }
    }

    register(data) {
        return axios.post('/api/user/register', data).then(res => {

        });
    }

    hasToken() {
        const storedToken = AppStorage.getToken();
        if (storedToken) {
            return Token.isValid(storedToken)
        }
        return false;
    }

    loggedIn() {
        return this.hasToken();
    }

    logout() {
        AppStorage.clear();
    }

    name() {
        return this.loggedIn() ? AppStorage.getUser() : false;
    }

    id() {
        return this.loggedIn() ? Token.decode(Token.payload(AppStorage.getToken())).sub : false;
    }


}

export default User = new User();

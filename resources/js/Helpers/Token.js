class Token {
    payload(token) {
        return token.split(".")[1];
    }
    decode(payload) {
        return JSON.parse(atob(payload));
    }
    isValid(token) {
        const payload = this.decode(this.payload(token));
        if (payload) {
            return payload.iss == 'http://realtimeapp.mydev/api/auth/login';
        }
        return false;
    }
}

export default Token = new Token();

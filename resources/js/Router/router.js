// 0. If using a module system (e.g. via vue-cli), import Vue and VueRouter
// and then call `Vue.use(VueRouter)`.
import Vue from "vue"
import VueRouter from "vue-router";

// Use
Vue.use(VueRouter);

// 1. Define route components.
// These can be imported from other files

import Login from "../components/login/Login";
import Logout from "../components/login/Logout";
import Forum from "../components/Forum";
import Register from "../components/login/Register";
import QuestionDetail from "../components/QuestionDetail";
import Ask from "../components/Ask";
import Edit from "../components/Edit";
import Categories from "../components/Categories";
const Home = { template: '' }

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
    { path: '/login', component: Login },
    { path: '/ask', component: Ask },
    { path: '/logout', component: Logout },
    { path: '/register', component: Register },
    { path: '/question/:id', component: QuestionDetail },
    { path: '/edit/:id', component: Edit, name: 'edit' },
    { path: '/', component: Home },
    { path: '/forum', component: Forum, name: 'forum' },
    { path: '/categories', component: Categories, name: 'categories' },
    //{ path: '/bar', component: Bar }
];

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
    routes, // short for `routes: routes`
    hashbang : false,
    mode : 'history'

});


export default router;

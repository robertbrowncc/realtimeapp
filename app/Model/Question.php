<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;



/**
 * App\Model\Question
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $body
 * @property int $category_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Model\Category $category
 * @property-read string $path
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Reply[] $replies
 * @property-read int|null $replies_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Question newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Question newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Question query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Question whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Question whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Question whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Question whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Question whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Question whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Question whereUserId($value)
 * @mixin \Eloquent
 */
class Question extends Model
{
    protected $fillable = [
        'title',
        'category_id',
        'body',
        'slug',
        'user_id'
    ];

    protected $with = ['replies'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function replies(): HasMany
    {
        return $this->hasMany(Reply::class)->orderByDesc('id');
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return string
     */
    public function getPathAttribute(): string
    {
        return asset('/api/question/' . $this->id);
    }

    /**
     * Register model events
     */
    protected static function boot()
    {
        parent::boot();
        self::saving(static function($question){
            $question->slug = \Str::slug($question->title);
            $question->user_id = auth()->user() ? auth()->user()->id : null;
        });
    }
}


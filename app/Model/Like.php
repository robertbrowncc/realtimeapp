<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Model\Like
 *
 * @property int $id
 * @property int $reply_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Model\Reply $reply
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Like newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Like newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Like query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Like whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Like whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Like whereReplyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Like whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Like whereUserId($value)
 * @mixin \Eloquent
 */
class Like extends Model
{
    protected $fillable = ['reply_id', 'user_id'];
    /**
     * @return BelongsTo
     */
    public function reply(): BelongsTo
    {
        return $this->belongsTo(Reply::class);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}

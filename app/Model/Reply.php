<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Model\Reply
 *
 * @property int $id
 * @property string $body
 * @property int $question_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Like[] $likes
 * @property-read int|null $likes_count
 * @property-read \App\Model\Question $question
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Reply newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Reply newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Reply query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Reply whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Reply whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Reply whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Reply whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Reply whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Reply whereUserId($value)
 * @mixin \Eloquent
 */
class Reply extends Model
{
    protected $fillable = ['body', 'user_id', 'question_id'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function likes(): HasMany
    {
        return $this->hasMany(Like::class);
    }

    /**
     * @return BelongsTo
     */
    public function question(): BelongsTo
    {
        return $this->belongsTo(Question::class);
    }

    /**
     * Register model events
     */
    protected static function boot()
    {
        parent::boot();
        self::saving(static function ($reply) {
            $reply->user_id = auth()->user()->id;
        });
    }
}

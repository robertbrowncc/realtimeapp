<?php

namespace App\Http\Requests;

use App\Model\Question;
use Illuminate\Foundation\Http\FormRequest;

class QuestionDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $question = $this->question;
        return $question && auth()->user()->id === $question->user_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //
        ];
    }
}

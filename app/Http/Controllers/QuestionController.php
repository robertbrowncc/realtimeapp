<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionDeleteRequest;
use App\Http\Requests\QuestionStoreRequest;
use App\Http\Requests\QuestionUpdateRequest;
use App\Http\Resources\QuestionResource;
use App\Model\Question;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Str;


class QuestionController extends Controller
{

    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        //return QuestionResource::collection(Question::query()->orderByDesc('id')->paginate(5));
        return QuestionResource::collection(Question::query()->orderByDesc('id')->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return QuestionResource
     */
    public function store(QuestionStoreRequest $request): QuestionResource
    {
        $question = auth()->user()->questions()->create($request->all());
        return new QuestionResource($question);
    }

    /**
     * Display the specified resource.
     *
     * @param Question $question
     * @return QuestionResource
     */
    public function show(Question $question): QuestionResource
    {
        return new QuestionResource($question);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param QuestionUpdateRequest $request
     * @param Question $question
     * @return QuestionResource
     */
    public function update(QuestionUpdateRequest $request, Question $question): QuestionResource
    {
        $params = $request->all();
        $question->update($params);
        return new QuestionResource($question);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param QuestionDeleteRequest $request
     * @param Question $question
     * @return Response
     * @throws Exception
     */
    public function destroy(QuestionDeleteRequest $request, Question $question): Response
    {
        $question->delete();
        return new Response('', Response::HTTP_NO_CONTENT);
    }
}

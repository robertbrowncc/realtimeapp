<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class UserController extends Controller
{
    /**
     * @param Request $request
     * @return UserResource|Response
     */
    public function register(RegisterRequest $request) {

        $params = $request->all();

        $params['password'] = bcrypt($params['password']);
        $user = User::create($params);
        return new UserResource($user);
    }
}

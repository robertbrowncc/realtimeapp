<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategorySaveRequest;
use App\Http\Resources\CategoryResource;
use App\Model\Category;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return CategoryResource::collection(Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategorySaveRequest $request
     * @return CategoryResource
     */
    public function store(CategorySaveRequest $request): CategoryResource
    {
        return new CategoryResource(Category::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return CategoryResource
     */
    public function show(Category $category): CategoryResource
    {
        return new CategoryResource($category);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param CategorySaveRequest $request
     * @param Category $category
     * @return CategoryResource
     */
    public function update(CategorySaveRequest $request, Category $category): CategoryResource
    {
        $category->update($request->all());
        return new CategoryResource($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return Response
     * @throws Exception
     */
    public function destroy(Category $category): Response
    {
        $category->delete();
        return new Response('', Response::HTTP_NO_CONTENT);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReplySaveRequest;
use App\Http\Resources\ReplyResource;
use App\Model\Question;
use App\Model\Reply;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class ReplyController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Question $question
     * @return AnonymousResourceCollection
     */
    public function index(Question $question): AnonymousResourceCollection
    {
        return ReplyResource::collection($question->replies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Question $question
     * @param ReplySaveRequest $request
     * @return ReplyResource
     */
    public function store(Question $question, ReplySaveRequest $request): ReplyResource
    {
        $params = $request->all();
        $params['question_id'] = $question->id;
        $reply = $question->replies()->create($params);
        return new ReplyResource($reply);
    }

    /**
     * Display the specified resource.
     *
     * @param Question $question
     * @param Reply $reply
     * @return ReplyResource
     */
    public function show(Question $question, Reply $reply): ReplyResource
    {
        return new ReplyResource($reply);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Question $question
     * @param Reply $reply
     * @param ReplySaveRequest $request
     * @return ReplyResource
     */
    public function update(Question $question, Reply $reply, ReplySaveRequest $request): ReplyResource
    {
        $reply->update($request->all());
        return new ReplyResource($reply);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Question $question
     * @param Reply $reply
     * @return Response
     * @throws Exception
     */
    public function destroy(Question $question, Reply $reply)
    {
        $reply->delete();
        return new Response('', Response::HTTP_NO_CONTENT);
    }
}

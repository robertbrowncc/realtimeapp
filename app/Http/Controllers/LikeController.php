<?php

namespace App\Http\Controllers;

use App\Http\Requests\LikeRequest;
use App\Http\Resources\LikeResource;
use App\Http\Resources\ReplyResource;
use App\Model\Like;
use App\Model\Question;
use App\Model\Reply;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class LikeController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Question $question
     * @param Reply $reply
     * @return AnonymousResourceCollection
     */
    public function index(Question $question, Reply $reply): AnonymousResourceCollection
    {
        return LikeResource::collection($reply->likes());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Question $question
     * @param Reply $reply
     * @param LikeRequest $request
     * @return ReplyResource
     */
    public function store(Question $question, Reply $reply, LikeRequest $request): ReplyResource
    {

        $like = new Like([
            'user_id' => auth()->user()->id,
            'reply_id' => $reply->id
        ]);
        $like->save();

        return new ReplyResource($reply);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Question $question
     * @param Reply $reply
     * @param LikeRequest $request
     * @return ReplyResource
     * @throws Exception
     */
    public function destroy(Question $question, Reply $reply, LikeRequest $request): ReplyResource
    {
        $like = Like::query()->where('user_id', auth()->user()->id)->where('reply_id', $reply->id)->first();
        if ($like) {
            $like->delete();
        }
        return new ReplyResource($reply);
    }
}

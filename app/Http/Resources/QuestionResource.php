<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'path' => $this->path,
            'body' => $this->body,
            'category_id' => $this->category_id,
            'created_at' => $this->created_at->diffForHumans(),
            'user_id' => $this->user->id,
            'user' => $this->user->name,
            'replies' => ReplyResource::collection($this->replies)
        ];
    }
}

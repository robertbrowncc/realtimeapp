<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ReplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'body' => $this->body,
            'question_id' => $this->question_id,
            'user_id' => $this->user_id,
            'user' => $this->user->name,
            'like_count' => $this->likes()->count(),
            'liked' => auth()->user() ? (bool)$this->likes()->where('user_id', auth()->user()->id)->count() : false,
            'created_at' => $this->created_at->diffForHumans()
        ];
    }
}
